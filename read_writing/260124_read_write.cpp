#include <fstream>
#include <iostream>
#include <iomanip>
using namespace std;
int q = 45;


int main() {
  const char* filename = "num.txt";
  ifstream Mynum(filename);
  ofstream Mysum("sum.txt");
  
  if(!Mynum) {
    cout << endl << "Failed to open file " << filename;
    return 1;
  }

  long n = 0;
  while(!Mynum.eof()) {
    Mynum >> n;
    cout <<"\nThis Number"<< setw(10) << n <<" + 45 = "<< n+q;
    Mysum << n+q << endl;
  }
  cout << endl;
  return 0;
}
